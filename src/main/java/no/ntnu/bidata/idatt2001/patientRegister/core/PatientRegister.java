package no.ntnu.bidata.idatt2001.patientRegister.core;

import no.ntnu.bidata.idatt2001.patientRegister.utils.CSVBroker;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the patient register itself.
 */
public class PatientRegister {
    private final List<no.ntnu.bidata.idatt2001.patientRegister.core.Patient> patientList;
    private CSVBroker reader;
    private CSVBroker writer;

    /**
     * Constructor for the class PatientRegister.
     */
    public PatientRegister(){
        patientList = new ArrayList();
    }

    /**
     * Plain getter.
     * @return the registers list of patients.
     */
    public List<no.ntnu.bidata.idatt2001.patientRegister.core.Patient> getPatientList(){
        return patientList;
    }

    /**
     * Adds patient to the register, as long as the register does not contain an equal object.
     * Can not be null. This method does not work without the custom equals in Patient.java
     * @see no.ntnu.bidata.idatt2001.patientRegister.core.Patient
     * @param patient Patient to be added to the register.
     * @return Boolean indicating successful or unsuccessful completion of the addition.
     */
    public boolean addPatient(no.ntnu.bidata.idatt2001.patientRegister.core.Patient patient){
        if(!patientList.contains(patient) && patient!=null){
            patientList.add(patient);
            return true;
        }
        return false;
    }

    /**
     * Adds patients imported from a .CSV-file.
     * Utilizes private method importFromCSV.
     * @param fileName name of the .CSV-file.
     */
    public void addPatientsFromCSV(String fileName){
        List<no.ntnu.bidata.idatt2001.patientRegister.core.Patient> newPatients = importFromCSV(fileName);
        patientList.addAll(newPatients);
    }

    /**
     * Saves the current patientList as a .CSV-file.
     * @see CSVBroker
     * @param fileName name of the .CSV-file
     */
    public void writePatientsToCSV(String fileName){
        writer = new CSVBroker();
        writer.writeToCSV(fileName, patientList);
    }

    /**
     *
     * @param patient
     */
    public boolean removePatient(no.ntnu.bidata.idatt2001.patientRegister.core.Patient patient){
        if(patientList.contains(patient) && patient != null){
            patientList.remove(patient);
            return true;
        }
        return false;
    }

    /**
     * Helping method checking that the file being read is a .CSV-file.
     * @see CSVBroker
     * @param fileName name of the .CSV-file
     * @return list of patients from the .CSV-file
     */
    private List<no.ntnu.bidata.idatt2001.patientRegister.core.Patient> importFromCSV(String fileName){
        reader = new CSVBroker();
        List<no.ntnu.bidata.idatt2001.patientRegister.core.Patient> newPatients = null;
        if(fileName.substring(fileName.length()-3).equals("csv")){
            newPatients = reader.readFromCSV(fileName);
        }
        return newPatients;
    }

    /**
     * Edits the values of a patient.
     * @param patient patient to be edited
     * @param newFirstName new first name
     * @param newLastName new last name
     * @param newSocialSecurityNumber new social security number
     */
    public boolean editPatient(no.ntnu.bidata.idatt2001.patientRegister.core.Patient patient, String newFirstName, String newLastName, String newSocialSecurityNumber) {
        if(patientList.contains(patient)){
            int index = patientList.indexOf(patient);
            no.ntnu.bidata.idatt2001.patientRegister.core.Patient editedPatient = new no.ntnu.bidata.idatt2001.patientRegister.core.Patient(newFirstName, newLastName, newSocialSecurityNumber);
            patientList.set(index, editedPatient);
            return true;
        }
        return false;
    }

    /**
     * Method displaying the contents of the register. Used for debugging.
     * @return String containing the objects in the register.
     */
    @Override
    public String toString(){
        StringBuilder out = new StringBuilder();
        for(no.ntnu.bidata.idatt2001.patientRegister.core.Patient patient : patientList){
            out.append(patient).append("\n");
        }
        return out.toString();
    }
}