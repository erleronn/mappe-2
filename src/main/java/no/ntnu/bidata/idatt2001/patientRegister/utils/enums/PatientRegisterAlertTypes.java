package no.ntnu.bidata.idatt2001.patientRegister.utils.enums;

/**
 * Enum representing types of alerts to be created in the alert factory.
 */
public enum PatientRegisterAlertTypes {
    CONFIRM_EXIT,
    ADDITION_COMPLETE,
    ADDITION_NOT_COMPLETE,
    EDIT_COMPLETE,
    EDIT_NOT_COMPLETE,
    PATIENT_NOT_SELECTED,
    DELETE_ARE_YOU_SURE,
    DELETION_COMPLETE,
    DELETION_NOT_COMPLETE,
    INVALID_INPUT,
    ABOUT,
    FILE_NOT_CSV
}
