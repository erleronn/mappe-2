package no.ntnu.bidata.idatt2001.patientRegister.gui;

import javafx.scene.control.Alert;
import no.ntnu.bidata.idatt2001.patientRegister.utils.enums.PatientRegisterAlertTypes;

/**
 * Class representing an alert factory. Utilizing factory design pattern in GUI.
 */
public class AlertFactory {
    public Alert getAlertType(PatientRegisterAlertTypes type){
        switch(type){
            case DELETE_ARE_YOU_SURE:
                Alert deleteConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
                deleteConfirmationAlert.setTitle("Warning");
                deleteConfirmationAlert.setHeaderText("Delete patient");
                deleteConfirmationAlert.setContentText("Are you sure you want to delete this patient?");
                return deleteConfirmationAlert;

            case CONFIRM_EXIT:
                Alert exitConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
                exitConfirmationAlert.setTitle("Exit");
                exitConfirmationAlert.setHeaderText("Are you sure you want to exit?");
                exitConfirmationAlert.setContentText("Save your work by exporting it as a .CSV-file!");
                return exitConfirmationAlert;


            case ADDITION_COMPLETE:
                Alert additionCompleteAlert = new Alert(Alert.AlertType.INFORMATION);
                additionCompleteAlert.setTitle("Add patient");
                additionCompleteAlert.setHeaderText("Success!");
                additionCompleteAlert.setContentText("Patient was added!");
                return additionCompleteAlert;

            case ADDITION_NOT_COMPLETE:
                Alert additionNotCompleteAlert = new Alert(Alert.AlertType.INFORMATION);
                additionNotCompleteAlert.setTitle("Add patient");
                additionNotCompleteAlert.setHeaderText("Not successful!");
                additionNotCompleteAlert.setContentText("Patient was not added!");
                return additionNotCompleteAlert;

            case EDIT_COMPLETE:
                Alert editCompleteAlert = new Alert(Alert.AlertType.INFORMATION);
                editCompleteAlert.setTitle("Edit patient");
                editCompleteAlert.setHeaderText("Success!");
                editCompleteAlert.setContentText("Patient was successfully edited.");
                return editCompleteAlert;

            case EDIT_NOT_COMPLETE:
                Alert editNotCompleteAlert = new Alert(Alert.AlertType.ERROR);
                editNotCompleteAlert.setTitle("Error");
                editNotCompleteAlert.setHeaderText("Edit not successful!");
                editNotCompleteAlert.setContentText("Patient not found.");
                return editNotCompleteAlert;

            case PATIENT_NOT_SELECTED:
                Alert patientNotSelectedAlert = new Alert(Alert.AlertType.ERROR);
                patientNotSelectedAlert.setTitle("Error");
                patientNotSelectedAlert.setHeaderText("Selected patient could not be loaded!");
                patientNotSelectedAlert.setContentText("Please select a patient you wish to edit, and try again!");
                return patientNotSelectedAlert;

            case DELETION_COMPLETE:
                Alert patientDeletedAlert = new Alert(Alert.AlertType.INFORMATION);
                patientDeletedAlert.setTitle("Delete patient");
                patientDeletedAlert.setHeaderText("Success!");
                patientDeletedAlert.setContentText("Patient was deleted.");
                return patientDeletedAlert;

            case DELETION_NOT_COMPLETE:
                Alert patientNotDeletedAlert = new Alert(Alert.AlertType.INFORMATION);
                patientNotDeletedAlert.setTitle("Delete patient");
                patientNotDeletedAlert.setHeaderText("Not successful!");
                patientNotDeletedAlert.setContentText("Patient was not deleted.");
                return patientNotDeletedAlert;

            case FILE_NOT_CSV:
                Alert fileNotCSVAlert = new Alert(Alert.AlertType.INFORMATION);
                fileNotCSVAlert.setTitle("Error");
                fileNotCSVAlert.setHeaderText("Patients not imported!");
                fileNotCSVAlert.setContentText("Please select a .CSV-file.");
                return fileNotCSVAlert;

            case INVALID_INPUT:
                Alert invalidInputAlert = new Alert(Alert.AlertType.INFORMATION);
                invalidInputAlert.setTitle("Error");
                invalidInputAlert.setHeaderText("Input not valid!");
                invalidInputAlert.setContentText("Remeber that social security numbers have 11 digits!\nNames can't contain numbers.");
                return invalidInputAlert;

            case ABOUT:
                Alert aboutAlert = new Alert(Alert.AlertType.INFORMATION);
                aboutAlert.setTitle("About");
                aboutAlert.setHeaderText("Patient register\nv1.0-SNAPSHOT");
                aboutAlert.setContentText("An application created by\n(C)Erlend Rønning\n04.05.2021");
                return aboutAlert;

            default:
                return null;
        }
    }
}
