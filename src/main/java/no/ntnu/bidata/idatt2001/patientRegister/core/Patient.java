package no.ntnu.bidata.idatt2001.patientRegister.core;

import java.util.Objects;

/**
 * Class representing a Patient in the patient register.
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;


    /**
     * Constructor for the class Patient. Creates an instance of the class.
     * Because of the given
     * @param firstName Patients first name
     * @param lastName Patients last name
     * @param socialSecurityNumber Patients social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) throws IllegalArgumentException{ //throws unchecked exception is optional
        if(isChars(firstName)){
            this.firstName = Objects.requireNonNull(firstName);
        } else {
            throw new IllegalArgumentException();
        }

        if(isChars(lastName)){
            this.lastName = Objects.requireNonNull(lastName);
        } else {
            throw new IllegalArgumentException();
        }

        if(isNotChars(socialSecurityNumber) && socialSecurityNumber.length() == 11){
            this.socialSecurityNumber = Objects.requireNonNull(socialSecurityNumber);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Constructor for the class Patient. Takes an array of strings as the parameter.
     * Used to create objects from .CSV-files. Because of varying lengths of the social security numbers in the
     * CSV file used in the development of the CSVBroker, it has no length check.
     * @param metadata String array containing the required fields.
     */
    public Patient(String[] metadata) {
        if(!(metadata[0]).isBlank()){
            this.firstName = Objects.requireNonNull(metadata[0]);
        }
        else {
            throw new IllegalArgumentException();
        }

        if(!(metadata[1]).isBlank()){
            this.lastName = Objects.requireNonNull(metadata[1]);
        } else {
            throw new IllegalArgumentException();
        }

        if(!(metadata[3]).isBlank()){
            this.socialSecurityNumber = Objects.requireNonNull(metadata[3]);
        } else {
            throw new IllegalArgumentException();
        }
    }

    //JPA requires no-args constructor
    public Patient() {
    }

    //Getters
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getDiagnosis() {
        return diagnosis;
    }
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }


    //Setters
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Checks weather a string is exclusively chars.
     * @param string to be checked
     * @return boolean depending on the result.
     */
    private boolean isChars(String string) {
        char[] chars = string.replaceAll("\\s+","").toCharArray();

        for (char c : chars) {
            if(!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks weather a string is exclusively not chars.
     * @param string to be checked
     * @return boolean depending on the result.
     */
    private boolean isNotChars(String string) {
        char[] chars = string.toCharArray();

        for (char c : chars) {
            if(Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Custom equals method. Only compares social security numbers, as patients could have the same name,
     * but never the same social security number.
     * @param o Object to be compared
     * @return Boolean representing the result of the comparison
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return getSocialSecurityNumber().equals(patient.getSocialSecurityNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSocialSecurityNumber());
    }

    @Override
    public String toString() {
        return lastName + "," + firstName + " | " + socialSecurityNumber;
    }
}
