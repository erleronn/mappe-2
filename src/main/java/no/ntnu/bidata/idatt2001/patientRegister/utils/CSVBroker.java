package no.ntnu.bidata.idatt2001.patientRegister.utils;

import no.ntnu.bidata.idatt2001.patientRegister.core.Patient;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import com.opencsv.CSVWriter;

/**
 * Class for reading to and writing from .CSV-files.
 */
public class CSVBroker {
    /**
     * Uses bufferedReader to create Patient-objects from a .CSV-file.
     * @param fileName .CSV-file
     * @return list containing the Patient-objects
     */
    public List<Patient> readFromCSV(String fileName){
        Path path = Paths.get(fileName);
        List<Patient> patients = new ArrayList();

        try(BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)){
            String line = reader.readLine();

            while(line != null){
                String[] values = line.split(";");
                Patient newPatient = new Patient(values);
                patients.add(newPatient);
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return patients;
    }

    /**
     * Utilizes the OpenCSV-API to write the patients in the list taken as an argument
     * to a .CSV-file.
     * @param fileName name of the .CSV file to be created.
     * @param patients list of patients to be written to the .CSV-file.
     */
    public void writeToCSV(String fileName, List<Patient> patients){
        File file = new File(fileName);
        List<String[]> patientsToCSV = new ArrayList<>();

        try(FileWriter outputFile = new FileWriter(file)){
            CSVWriter writer = new CSVWriter(outputFile, ';',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

            for(Patient patient : patients){
                String[] data = {patient.getFirstName(),patient.getLastName(), "", patient.getSocialSecurityNumber()};
                patientsToCSV.add(data);
            }

            writer.writeAll(patientsToCSV);
            writer.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
