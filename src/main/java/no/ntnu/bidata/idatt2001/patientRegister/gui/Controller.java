package no.ntnu.bidata.idatt2001.patientRegister.gui;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import no.ntnu.bidata.idatt2001.patientRegister.core.Patient;
import no.ntnu.bidata.idatt2001.patientRegister.core.PatientRegister;
import no.ntnu.bidata.idatt2001.patientRegister.utils.enums.PatientRegisterAlertTypes;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private TableView<Patient> table;
    @FXML
    private TableColumn<Patient,String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumberColumn;
    @FXML
    private StackPane addDialogPane;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField socialSecurityNumberField;

    @FXML
    private StackPane editDialogPane;
    @FXML
    private TextField editFirstNameField;
    @FXML
    private TextField editLastNameField;
    @FXML
    private TextField editSocialSecurityNumberField;

    @FXML
    private Button editButton;
    @FXML
    private Button cancelEditButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;

    private no.ntnu.bidata.idatt2001.patientRegister.gui.AlertFactory alertFactory;

    private ObservableList<Patient> tableList;

    PatientRegister register;

    /**
     * Takes in a .CSV-file to read patients from by utilizing JFileChooser.
     */
    public void importFromCSV(){
        JFileChooser fc = new JFileChooser(".");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.csv", "csv");
        fc.setFileFilter(filter);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        try{
            int response = fc.showOpenDialog(null);
            if(response == JFileChooser.APPROVE_OPTION){
                register.addPatientsFromCSV(fc.getSelectedFile().getAbsolutePath());
                updateTables();
            }
        } catch (NullPointerException e){
            Alert catchAlert = alertFactory.getAlertType(PatientRegisterAlertTypes.FILE_NOT_CSV);
            catchAlert.showAndWait();
        }

    }

    /**
     * Prompts the user to select folder and choose a name for the output .CSV-file.
     */
    public void writeToCSV(){
        JFileChooser fc = new JFileChooser(".");
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setDialogTitle("Save as...");
        int response = fc.showSaveDialog(null);
        System.out.println(fc.getSelectedFile().toPath());

        if(!(fc.getSelectedFile().toString().endsWith(".csv"))){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Export file");
            alert.setHeaderText("Error, wrong file format!");
            alert.setContentText("File was not successfully exported.");

            alert.showAndWait();
            return;
        }

        register.writePatientsToCSV(fc.getSelectedFile().getAbsolutePath());
        File file = new File(fc.getSelectedFile().toString());
        if(file.exists()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Export file");
            alert.setHeaderText("Success!");
            alert.setContentText("File was successfully exported.");

            alert.showAndWait();
        } else {
            System.out.println("shit");
        }

    }

    /**
     * Updates the tables contents.
     */
    private void updateTables(){
        this.tableList.setAll(this.register.getPatientList());
        this.table.getItems().setAll(tableList);
    }

    /**
     * Initializes the patient register as well as the table columns.
     * Adds event handlers to the buttons in the dialogs.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.register = new PatientRegister();

        this.alertFactory = new no.ntnu.bidata.idatt2001.patientRegister.gui.AlertFactory();

        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        this.socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        this.tableList = FXCollections.observableArrayList(this.register.getPatientList());

        this.table.setItems(FXCollections.observableArrayList(this.tableList));

        initAddPatientEventHandlers();
        initEditPatientEventHandler();
    }

    /**
     * Method called by clicking Edit -> Add patient, or the icon.
     */
    public void addPatient(){
        addDialogPane.setVisible(true);
    }

    /**
     * Method called by saving a patient from the add patient dialog.
     * Takes in the text in the fields and creates a patient object from these.
     */
    private void savePatient() {
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String socialSecurityNumber = socialSecurityNumberField.getText();

        try{
            Patient addPatient = new Patient(firstName,lastName,socialSecurityNumber);
            if(register.addPatient(addPatient)) {
                Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.ADDITION_COMPLETE);

                alert.showAndWait();
                closeDialog();
            }
        } catch (IllegalArgumentException e){
            Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.INVALID_INPUT);

            alert.showAndWait();
        }

        updateTables();

    }

    /**
     * Uses the selection model in TableView to delete a patient from the register.
     * Requires user confirmation to go through with deletion.
     */
    public void deletePatient(){
        Patient toBeRemoved = table.getSelectionModel().getSelectedItem();
        if(toBeRemoved == null){
            Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.DELETION_NOT_COMPLETE);

            alert.showAndWait();
        } else if(register.getPatientList().contains(toBeRemoved)){
            Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.DELETE_ARE_YOU_SURE);

            Optional<ButtonType> result = alert.showAndWait();
            if(result.isEmpty()){
                alert.close();
                // Alert is exited, no button has been pressed
            } else if(result.get() == ButtonType.OK){
                if(register.removePatient(toBeRemoved)){
                    Alert alert1 = alertFactory.getAlertType(PatientRegisterAlertTypes.DELETION_COMPLETE);
                    alert1.showAndWait();
                } else {
                    Alert alert2 = alertFactory.getAlertType(PatientRegisterAlertTypes.DELETION_NOT_COMPLETE);
                    alert2.showAndWait();
                }
                // OK-button is pressed
            } else if(result.get() == ButtonType.CANCEL){
                alert.close();
                // Cancel-button is pressed
            }
        }
        updateTables();
    }

    /**
     * Closes the dialogs based on the boolean isVisible.
     */
    private void closeDialog(){
        if(addDialogPane.isVisible()){
            addDialogPane.setVisible(false);
        } else if (editDialogPane.isVisible())
            editDialogPane.setVisible(false);
        clearDialog();
    }

    /**
     * Method called by clicking Edit -> Edit selected patient, or the edit icon.
     * Uses the selection model in TableView to select patient to be edited.
     * Sets the fields in the edit dialog to the ones in the object to be edited.
     */
    public void editPatient(){
        if((table.getSelectionModel().getSelectedItem()) == null){
            Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.PATIENT_NOT_SELECTED);

            alert.showAndWait();
        } else{
            Patient edit = table.getSelectionModel().getSelectedItem();
            editDialogPane.setVisible(true);
            editFirstNameField.setText(edit.getFirstName());
            editLastNameField.setText(edit.getLastName());
            editSocialSecurityNumberField.setText(edit.getSocialSecurityNumber());
        }
    }

    /**
     * Method called by clicking the edit button.
     * Edits the patient to contain the fields inputted in the dialog.
     */
    private void saveEdit(){
        if(register.editPatient(table.getSelectionModel().getSelectedItem(),
                editFirstNameField.getText(), editLastNameField.getText(),
                editSocialSecurityNumberField.getText())){
            Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.EDIT_COMPLETE);

            alert.showAndWait();
        } else {
            Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.EDIT_NOT_COMPLETE);

            alert.showAndWait();
        }
        updateTables();
        closeDialog();
    }

    /**
     * Clears the contents of the textFields.
     */
    private void clearDialog(){
        firstNameField.clear();
        lastNameField.clear();
        socialSecurityNumberField.clear();
        editFirstNameField.clear();
        editLastNameField.clear();
        editSocialSecurityNumberField.clear();
    }

    /**
     * Adds event handlers to the buttons in the add patient dialog.
     */
    private void initAddPatientEventHandlers(){
        cancelButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> closeDialog());
        saveButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> savePatient());
    }

    /**
     * Adds event handlers to the buttons in the add patient dialog.
     */
    private void initEditPatientEventHandler(){
        cancelEditButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> closeDialog());
        editButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> saveEdit());
    }

    /**
     * Exits the application after asking for confirmation.
     */
    public void exit(){
        Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.CONFIRM_EXIT);

        Optional<ButtonType> result = alert.showAndWait();
        if(result.isEmpty()){
            alert.close();
            // Alert is exited, no button has been pressed
        } else if(result.get() == ButtonType.OK){
            Platform.exit();
            // OK-button is pressed
        } else if(result.get() == ButtonType.CANCEL) {
            alert.close();
            // Cancel-button is pressed
        }
    }

    /**
     * Displays the about dialog.
     */
    public void about(){
        Alert alert = alertFactory.getAlertType(PatientRegisterAlertTypes.ABOUT);

        alert.showAndWait();
    }
}
