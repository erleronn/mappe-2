module no.ntnu.bidata.idatt2001 {
    requires javafx.graphics;
    requires javafx.fxml;
    requires opencsv;
    requires javafx.controls;
    requires java.desktop;
    exports no.ntnu.bidata.idatt2001.patientRegister;
    exports no.ntnu.bidata.idatt2001.patientRegister.utils.enums;
    exports no.ntnu.bidata.idatt2001.patientRegister.utils;
    exports no.ntnu.bidata.idatt2001.patientRegister.gui;
    exports no.ntnu.bidata.idatt2001.patientRegister.core;
    opens no.ntnu.bidata.idatt2001.patientRegister.core to javafx.base;
    opens no.ntnu.bidata.idatt2001.patientRegister.gui to javafx.fxml, javafx.controls, javafx.graphics;

}