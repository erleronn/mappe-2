import no.ntnu.bidata.idatt2001.patientRegister.core.Patient;
import no.ntnu.bidata.idatt2001.patientRegister.core.PatientRegister;
import no.ntnu.bidata.idatt2001.patientRegister.utils.CSVBroker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CSVBrokerTest {
    CSVBroker broker;
    PatientRegister register;

    @BeforeEach
    public void arrange(){
        register = new PatientRegister();
        broker = new CSVBroker();
    }

    @Test
    public void readFromCSVTest(){
        //Act
        List<Patient> patients = broker.readFromCSV(System.getProperty("user.dir") + "/src/main/resources/csv/Patients.csv");
        //Assert
        assertEquals(patients.size(), 105);
    }

    @Test
    public void readFromNullTest(){
        //Act and assert
        assertThrows(NullPointerException.class, () -> {
            List<Patient> patients = broker.readFromCSV(null);
        });
    }

    @Test
    public void writeToCSVTest(){
        //Arrange
        boolean exists;
        File file;
        //Act
        broker.writeToCSV("out.csv",register.getPatientList());
        file = new File("out.csv");
        exists = file.exists();
        //Assert
        assertTrue(exists);
        file.delete();
    }

    @Test
    public void writeToNullTest(){
        //Act and assert
        assertThrows(NullPointerException.class, () -> broker.writeToCSV(null,register.getPatientList()));
    }
}


