package core;

import no.ntnu.bidata.idatt2001.patientRegister.core.Patient;
import no.ntnu.bidata.idatt2001.patientRegister.core.PatientRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PatientRegisterTest {
    PatientRegister dummyRegister;
    Patient dummyPatient;
    Patient inRegister;
    Patient nullPatient;

    @BeforeEach
    public void arrange(){
        dummyRegister = new PatientRegister();
        inRegister = new Patient("in", "register", "11111111111");
        dummyPatient = new Patient("dummy", "patient", "22222222222");
        nullPatient = null;
        dummyRegister.addPatient(inRegister);
    }

    @Nested
    @DisplayName("Tests for the method addPatient")
    public class AddPatient{

        @Test
        public void addPatientToRegisterTest(){
            //Act
            boolean added = dummyRegister.addPatient(dummyPatient);
            //Assert
            assertTrue(added);
        }

        @Test
        public void addExistingPatientToRegisterTest(){
            //Act
            boolean added = dummyRegister.addPatient(inRegister);
            //Assert
            assertFalse(added);
        }

        @Test
        public void addNullToRegisterTest() {
            //Act
            boolean added = dummyRegister.addPatient(nullPatient);
            //Assert
            assertFalse(added);
        }
    }

    @Nested
    @DisplayName("Tests for the method removePatient")
    public class RemovePatient{

        @Test
        public void removeAddedPatientFromRegisterTest(){
            //Act
            boolean removed = dummyRegister.removePatient(inRegister);
            //Assert
            assertTrue(removed);
        }

        @Test
        public void removeNotAddedPatientFromRegisterTest(){
            //Act
            boolean removed = dummyRegister.removePatient(dummyPatient);
            //Assert
            assertFalse(removed);
        }

        @Test
        public void removeNullPatientFromRegisterTest(){
            //Act
            boolean removed = dummyRegister.removePatient(nullPatient);
            //Assert
            assertFalse(removed);
        }
    }

    @Nested
    @DisplayName("Tests for the method editPatient")
    public class EditPatient{

        @Test
        public void editPatientInRegisterTest(){
            //Act
            boolean edited = dummyRegister.editPatient(inRegister, "first name", "last name", "33333333333");
            //Assert
            assertTrue(edited);
        }

        @Test
        public void editPatientNotInRegister(){
            //Act
            boolean edited = dummyRegister.editPatient(dummyPatient, "first name", "last name", "33333333333");
            //Assert
            assertFalse(edited);
        }

        @Test
        public void editNullPatient(){
            //Act
            boolean edited = dummyRegister.editPatient(nullPatient, "first name", "last name", "33333333333");
            //Assert
            assertFalse(edited);
        }
    }
}
