package core;

import no.ntnu.bidata.idatt2001.patientRegister.core.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PatientTest {

    @BeforeEach
    public  void arrange(){

    }

    @Test
    public void constructorUsingStringArrayTest(){
        //Arrange
        Patient dummyPatient;
        Patient arrayPatient;
        // Act
        dummyPatient = new Patient("dummy", "patient", "11111111111");
        arrayPatient = new Patient(new String[]{"dummy", "patient", "", "11111111111"});
        //Assert
        assertEquals(dummyPatient,arrayPatient);
    }
}
